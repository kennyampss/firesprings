import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      // I want to stop dream attacks
      path: '/dreams-pg',
      name: 'dreams-pg',
      component: () => import( './views/dreams-pg.vue')
    },
      {
          // I need prayers to get out of debt
          path: '/debtfree-pg',
          name: 'debtfree-pg',
          component: () => import( './views/debtfree-pg.vue')
      },
      {
          // Stop Divorce/ Restor my marriage
          path: '/stopdivorce-pg',
          name: 'stopdivorce-pg',
          component: () => import( './views/stopdivorce-pg.vue')
      },
      {
          // Prayers for my godly spouse to locate me
          path: '/prayerforgodlyspouse-pg',
          name: 'prayerforgodlyspouse-pg',
          component: () => import( './views/prayerforgodlyspouse-pg.vue')
      },
      {
          // Prayers for new or better job
          path: '/prayerforbetterjob-pg',
          name: 'prayerforbetterjob-pg',
          component: () => import( './views/prayerforbetterjob-pg.vue')
      },
      {
          // Prayers for financial breakthrough
          path: '/prayerforfinancial-pg',
          name: 'prayerforfinancial-pg',
          component: () => import( './views/prayerforfinancial-pg.vue')
      },
      {
          // I need healing prayers
          path: '/healingprayer-pg',
          name: 'healingprayer-pg',
          component: () => import( './views/healingprayer-pg.vue')
      },
      {
          // Prayers for Salvation of loved ones
          path: '/prayerforsalvation-pg',
          name: 'prayerforsalvation-pg',
          component: () => import( './views/prayerforsalvation-pg.vue')
      },
      {
          // Prayers for Spiritual Growth
          path: '/prayerforspiritualgrowth-pg',
          name: 'prayerforspiritualgrowth-pg',
          component: () => import( './views/prayerforspiritualgrowth-pg.vue')
      },
      {
          // I want the 25 thanksgiving Prayers
          path: '/thanksgivingpartners-pg',
          name: 'thanksgivingpartners-pg',
          component: () => import( './views/thanksgivingpartners-pg.vue')
      },


  ]
})
