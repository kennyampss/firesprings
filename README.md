# firesprings

to push an update:

1. copy project folder to your drive
2. download and install nodejs https://nodejs.org/en/ (choose recommended for users)
3. install vuejs > run cmd and type: npm install -g @vue/cli
4. after nodejs installation > inside project folder run cmd and type: npm install
5. after npm installation > run cmd and type: npm run serve
6. that's it! you can change the data locally and it automatically loads in the browser without refreshing the page. 


To change the email receiver:

1. locate the code in src/assets/components/homepage/page_2.vue
2. find or search (youremailhere.com) within the code and change it to your email receiver

